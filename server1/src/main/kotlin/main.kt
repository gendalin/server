import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.net.NetServer
import io.vertx.core.net.NetServerOptions
import io.vertx.core.net.NetSocket
import kotlin.math.pow

fun main(){
    val vertx: Vertx = Vertx.vertx()
    val options: NetServerOptions = NetServerOptions().setPort(5432)
    options.setHost("localhost")
    val server: NetServer = vertx.createNetServer(options)
    var num: Int = 0
    server.connectHandler { socket: NetSocket ->
        var isAuth:Boolean = false
        var str = ""
        num += 1
        socket.handler { buffer: Buffer ->
            if (buffer.getByte(buffer.length()-1).toInt().toChar() != '\n' )
                str += buffer.toString()
            else
            {
                str += buffer.toString().dropLast(1)
                if (str.length > 3)
                {
                    val checkStr: String = str.take(3)
                    if (checkStr=="#L#")
                    {
                        val numStr: String= str.drop(3)
                        val numStrGar: String= str.drop(3).filter { it >= '0' && it<='9'}
                        if (numStr==numStrGar)
                        {
                            val sumElem = numStr.map{it.toString().toDouble()}
                                .let{it.zip(it.reversed()){m1,m2 ->m1.pow(m2)}}
                                .filterIndexed{index, s -> index%2==0}
                                .reduce { sum, element -> sum + element }
                            isAuth = sumElem.toInt()%2 == 0
                            if (isAuth)
                                socket.write("#L#1")
                            else
                                socket.write("#L#0")
                        }
                        else
                            socket.write("#BR")
                    }
                    else if(checkStr=="#D#")
                        if (isAuth)
                            socket.write("#OK#"+str.drop(3))
                        else
                            socket.write("#NA")
                    else
                        socket.write("#BR")
                }
                str = ""
            }
        }
        socket.closeHandler { v ->
            num -= 1
            if (num == 0){
                socket.close()
                vertx.close()
            }
        }
    }
    server.listen(5432, "localhost");
}